# **INF226 Compulsory Assignment 2+3**

## Design considerations:


#### Application setup:

I have improved the project structure by seperating the flask application into multiple files using blueprints:
- *app.py* now consist of mostly setup code. 
- *auth.py* handles login/out + registration.
- *msg.py* handles requests that consist of sending/recieving/searching trough messages.

<br>

### Database design:
I have created a table for users and another that handles messages. A message has [id, sender, reciever, message, date] and users have [id, username, saltedAndHashedPassword, accessToken]. A user can only see messages where they are the sender or the reciever.

<br>

## Security patches / implementations:

- SQL injections has been midigated by using prepared statements and tests have been ran using sqlmap.

- The Cross-site scripting vulnerability caused by the use of 'htmlContent' that could be abused by userinput has been patched.

- Cross-site request forgery attacks have been midigated by using the flask **GSRFProtect** library.

- Passwords are stored as salted hashes by using the **bcrypt** library.

<br>

## Security threats

- DDOS/Bot attacks, server flooding with data and or requests. Rate limiting user requests/connections and added rechapthas would midigate this issue.

- Unsecure *http* connection; a signed certificate is needed to encrypt the connection between the user and the server.

- Other unknown bugs and exploits



<br>

## How to test the app:
1. clone the repository [ `git clone git@git.app.uib.no:Vegard.Demring/messagingserver.git` ]
2. install dependences [ `pip install flask flask_wtf flask_login` ]
3. run the [ `flask run` ] command
4. go to http://127.0.0.1:5000/
5. create an account (ideally two unless you want to msg only your own account which is possible) Username and Passwords have certain requirements such as length, complexety and allowed characters.
6. login to your newly created account
7. send a message to yourself or another account
8. try the search function to search for your earlier messages using message content or date/time.
9. try logging out with the use of the logout button placed at the top of the page

<br>

## 2B Questions
Threat model – who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?
- confidentiality - The attacker could steal other users passwords and read their messages. The user messages could contain personal information that should not be shared.

- integrity - A attacker could alter the contents of other people messages and thereby have the potential to pose as someone else or paint an image of/frame someone (Man-in-the-middle attack).

- Availability - The attacker can potentially cause downtime. They can spam the server with loads of messages/requests and that might potentially either overload it or make it drop other legitimate user sessions. Another sideffect would be the increased storage/bandwith cost in a real deployment. 

What are the main attack vectors for the application?
- See [**Security threats**](#security-threats)

What should we do (or what have you done) to protect against attacks?
- See [**Security patches / implementations**](#security-patches--implementations) 
 & [**Security threats**](#security-threats)

What is the access control model?
- Users should only have access to their own messages so a DAC model is in place.

How can you know that you security is good enough? (traceability)
- Is is hard to tell when an application has "good enough" secuirty, with good traceability you can find if, when and how someone exploited your application. Security always comes with a cost. Whether it be developer cost or computational, but weak security often leads to even more costly huge fines and compromized information.

<br>

## Part 3

WIP