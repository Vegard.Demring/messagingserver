from http import HTTPStatus
from flask import Flask, abort
from werkzeug.datastructures import WWWAuthenticate
from flask_wtf.csrf import CSRFProtect
from flask_login import LoginManager, UserMixin
from base64 import b64decode

import sys
import apsw

import routes
import auth
import msg

# Set up app
app = Flask(__name__)

# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
# Change this before server deployment, as it is now visible on Git! Generate a key with "token_hex()"
app.secret_key = '56b9393f3b86e5009ba3a08ea2368278d0fc72d5b0bb69d69e047271fa2f74d1' 

# Registrer blueprints
app.register_blueprint(routes.app)
app.register_blueprint(auth.app)
app.register_blueprint(msg.app)


# Add a login manager to the appcsrf_token
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "auth.login"

# Add CSRF protection
csrf = CSRFProtect()
csrf.init_app(app)

# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(UserMixin):
    pass

# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    res = getConnection().execute("SELECT * FROM users WHERE id = ?",(user_id, ))
    row = res.fetchone()

    if (row is None):
        return None
    else:
        user = User()
        user.id = user_id
        user.username = row[1]
        return user

# Connection to the database
def getConnection():
    return apsw.Connection('./tiny.db')

# Bind to flask.current_app for blueprints
app.db = getConnection()
app.user_loader = user_loader

# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    authHeader = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not authHeader:
        return

    (auth_scheme, auth_params) = authHeader.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        print(f'Basic auth: {uid}:{passwd}')

        user = user_loader(uid)
        if (auth.checkPassword(user.username,passwd)):
            return user_loader(uid)

    elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        print(f'Bearer auth: {auth_params}')

        if auth.fetchToken(uid) == auth_params:
            return user_loader(uid)

    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))


try:
    c = getConnection().cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        receiver TEXT NOT NULL,
        message TEXT NOT NULL,
        date DATETIMEOFFSET NOT NULL);''')

    c.execute('''CREATE TABLE IF NOT EXISTS users (
        id integer PRIMARY KEY, 
        username TEXT NOT NULL UNIQUE,
        password TEXT NOT NULL,
        token TEXT NOT NULL UNIQUE);''')

except apsw.Error as e:
    print(e)
    sys.exit(1)
