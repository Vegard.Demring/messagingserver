from secrets import token_hex
from markupsafe import escape
from flask import Blueprint, redirect, render_template
from flask_login import login_required, login_user, logout_user
import flask
import apsw
import bcrypt

from flask import current_app
from forms import CreateUserForm, LoginForm

app = Blueprint("auth", __name__)

#Returns the user id if username and password is correct
def checkPassword(username, password):
    res = current_app.db.execute("SELECT * FROM users WHERE username = ?", (username, ))
    row = res.fetchone()
    if (not row):
        return False
    if (bcrypt.checkpw(password.encode('utf-8'), row[2].encode("utf-8"))):
        return row[0]
    return False

#Hashes and salts the plaintext password using bcrypt
def generatePasswordHash(plainPassword):
    hash = bcrypt.hashpw(plainPassword.encode("utf-8"), bcrypt.gensalt())
    return hash.decode('utf-8')

def fetchToken(uid):
    res = current_app.db.execute("SELECT * FROM users WHERE user_id = ?", (uid,))
    row = res.fetchone()
    return escape(row[3])

def isUsernameAvailable(username):
    res = current_app.db.execute("SELECT * FROM users WHERE username = ?", (username,))
    row = res.fetchone()
    return row is None


@app.route('/registrer', methods=['GET', 'POST'])
def registrer():
    form = CreateUserForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')

    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data

        if (isUsernameAvailable(username)):
            try:
                current_app.db.execute('INSERT INTO users (username, password, token) VALUES (?, ?, ?);', (username, generatePasswordHash(password), token_hex()))
                flask.flash("User Was Created")

                return flask.redirect("/login")
            except apsw.Error as e:
                return {'error': f'{e}'}
        else:
            flask.flash("Username Taken")
            form.form_errors.append("Username Taken")
            return render_template('./registrer.html', form=form)

    return render_template('./registrer.html', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')

    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        userid = checkPassword(username,password)

        if (userid):
            user = current_app.user_loader(userid)
            
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')
            next = flask.request.args.get('next')
    
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('routes.index_html'))
        else:
            flask.flash("Login Failed")
            form.form_errors.append("Login failed") #TODO notify user in a cleaner fachion
            return render_template('./login.html', form=form)
    return render_template('./login.html', form=form)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect("/login")