from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, validators


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[
        validators.Length(min=0, max=256)
    ])
    password = PasswordField(label='Password', validators=[
        validators.Length(min=0, max=64)
    ])
    submit = SubmitField('Submit')

class CreateUserForm(FlaskForm):
    username = StringField('Username', validators=[
        validators.Length(min=3, max=32),
        validators.Regexp('^[A-Za-z]*$', message = "Username can only consist of letters"),
    ])
    password = PasswordField(label='Password', validators=[
        validators.Length(min=6, max=64),
        validators.Regexp('^[^\s]*$',   message = "Password can not include whitespaces"),
        validators.Regexp('.*[A-Z].*',  message = "Password must contain at least a uppercase letter"),
        validators.Regexp('.*[a-z].*',  message = "Password must contain at least a lowercase letter"),
        validators.Regexp('.*[0-9].*',  message = "Password must contain at least a digit"),

        validators.EqualTo('repeatPassword', message='The given passwords does not match'),
    ])
    repeatPassword = PasswordField(label='Repeat Password', validators=[
        validators.Length(min=6, max=64)
    ])

    submit = SubmitField('Submit')

class SendMessageForm(FlaskForm):
    reciever = StringField( validators=[
        validators.Regexp('^[A-Z,]*$', message = "Username can only consist of CAPITAL letters"),
    ])
    message = StringField()

    submit = SubmitField('Send')
