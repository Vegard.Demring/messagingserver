from flask import Blueprint, current_app, request
from flask_login import current_user, login_required
from datetime import datetime

import apsw

from auth import isUsernameAvailable

app = Blueprint("msg", __name__)

@app.route('/send', methods=['POST','GET'])
@login_required
def send():
    try:
        sender = current_user.username
        reciever = request.args.get('receiver') or request.form.get('receiver')
        message = request.args.get('message') or request.args.get('message')
        date = datetime.now().isoformat()

        if not reciever or not message:
            return f'ERROR: missing reciever or message'

        errors = ""
        for recipient in reciever.split(","):
            recipient = recipient.replace(" ", "")
            if (isUsernameAvailable(recipient)):
                errors += f"\nThere is no such user with the name: '{recipient}'\n"
            else:
                current_app.db.execute('INSERT INTO messages (sender,receiver,message,date) VALUES (?,?,?,?);', (sender,recipient,message,date))
        return f'ERROR: {errors}'
    except apsw.Error as e:
        return f'ERROR: {e}'
  

@app.get('/search')
@login_required
def search():
    username = current_user.username
    query = request.args.get('q') or request.form.get('q')
    query = "*" + query + "*"
    try:
        c = current_app.db.execute("SELECT * FROM messages WHERE (sender = ? OR receiver = ?) AND (message GLOB ? OR date GLOB ?)",(username,username,query,query))
        rows = c.fetchall()
        result = ""
        for row in rows:
            result += f'\nFrom {row[1]} to {row[2]}:\n{row[3]}\n@{datetime.fromisoformat(row[4]).replace(microsecond=0)}\n'
        c.close()
        return result
    except apsw.Error as e:
        return (f'ERROR: {e}', 500)


@app.get('/recieve')
@login_required
def recieve():
    try:
        username = current_user.username
        c = current_app.db.execute("SELECT * FROM messages WHERE (sender = ? OR receiver = ?) ORDER BY date DESC LIMIT 10",(username,username))
        rows = c.fetchall()
        messages = []
        for row in rows:
            messages.append((row[1],row[2],row[3],datetime.fromisoformat(row[4]).replace(microsecond=0)))
        messages.reverse()
        return {'data': messages}
    except apsw.Error as e:
        return {'error': f'{e}'}
