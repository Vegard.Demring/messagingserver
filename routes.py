from pygments.formatters import HtmlFormatter
from flask import Blueprint, make_response, render_template, send_from_directory, abort
from flask_login import login_required

cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
app = Blueprint("routes", __name__)


@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return render_template("/index.html")

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, '/static/img/favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'static/img/favicon.png', mimetype='image/png')

@app.route('/coffee/', methods=['POST','PUT'])
def gotcoffee():
    return "Thanks!"

@app.get('/coffee/')
def nocoffee():
    abort(418)

@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp