var anchor = document.getElementById('anchor');
var searchField = document.getElementById('search');
var receiverField = document.getElementById('receiver');
var messageField = document.getElementById('message');
var searchBtn = document.getElementById('searchBtn');
var sendBtn = document.getElementById('sendBtn');
var output = document.getElementById('output');
var logoutBtn = document.getElementById('logoutBtn');
const msgs = [];

async function recieveMessages() {
    res = await fetch('/recieve',{ method: 'GET', credentials: 'same-origin'});
    recieve = await res.json();

    if (recieve && Array.isArray(recieve.data)) {
        recieve.data.forEach((msg) => {
            if (!msgs.includes(msg[0]+msg[1]+msg[2]+msg[3])){
                msgs.push(msg[0]+msg[1]+msg[2]+msg[3]);

                const body = document.createElement('p');
                body.innerText = `From ${msg[0]} to ${msg[1]}:\n${msg[2]}\n\n@${msg[3]}`;
                output.appendChild(body);
                body.scrollIntoView(false,{ block: "end", inline: "nearest", behavior: "smooth" });
            }
            if (msgs.length > 50){
                msgs.shift();
            }
        });
    }
};

async function search(query) {
    const q = `/search?q=${encodeURIComponent(query)}`;
    res = await fetch(q);
    
    const head = document.createElement('h3');
    head.textContent = `Your message history with the keywords '${query}':`;
    output.appendChild(head);

    const body = document.createElement('p');
    body.innerText = await res.text();
    output.appendChild(body);
    
    body.scrollIntoView(false,{ block: "end", inline: "nearest", behavior: "smooth" });
    anchor.scrollIntoView();
};

async function send(receiver, message) {
    var csrf_token = document.getElementById("csrf_token");
    if (!csrf_token){
        console.log("CSRF token is not present!");
        return
    }
    const q = `/send?receiver=${encodeURIComponent(receiver)}&message=${encodeURIComponent(message)}`;
    res = await fetch(q, { method: 'post', headers:{"X-CSRFToken": csrf_token.value} });  

    err = await res.text();
    if (err != ""){
        const body = document.createElement('p');
        body.innerText = err;
        output.appendChild(body);

        body.scrollIntoView(false,{ block: "end", inline: "nearest", behavior: "smooth" });
        anchor.scrollIntoView();
    }

    recieveMessages();
};

searchField.addEventListener('keydown', ev => {
    if (ev.key === 'Enter') {
        search(searchField.value);
    }
});
searchBtn.addEventListener('click', () => search(searchField.value));
sendBtn.addEventListener('click', () => send(receiverField.value, messageField.value));
logoutBtn.addEventListener('click', () => window.location.href = "/logout");

//Look for new messages every 5s
setInterval(recieveMessages,5000);
recieveMessages();